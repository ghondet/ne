### Examples of NE proofs.

#### Structure of the repo

- Examples/ : some proofs developed in NE.
- HOLLight/ : proofs from the HOL Light standard library.
- IsabelleHOL/ : selected proofs from the Isabelle/HOL standard library
- Matita/ : proofs from the arithmetic library of Matita.

#### Dependencies

- Dedukti 2.7, installation instructions are available at https://github.com/Deducteam/Dedukti
