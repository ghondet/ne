divides : ne.eta (hole.arrow nat.nat (hole.arrow nat.nat hole.o)).


quotient :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.forall
             ne.i
             nat.nat
             (q:(ne.eta nat.nat) =>
              ne.imp ne.i (logic.eq nat.nat m (nat.times n q)) (divides n m))))).


match_divides_prop :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.forall
             ne.i
             hole.o
             (return_type:(ne.eta hole.o) =>
              ne.imp
                ne.i
                (ne.forall
                   ne.i
                   nat.nat
                   (q:(ne.eta nat.nat) =>
                    ne.imp ne.i (logic.eq nat.nat m (nat.times n q)) return_type))
                (ne.imp ne.i (divides n m) return_type))))).


def reflexive_divides :
  ne.eps ne.i (relations.reflexive nat.nat divides)
  :=
  x:(ne.eta nat.nat) =>
  quotient
    x
    x
    (nat.S nat.O)
    (logic.rewrite_r
       nat.nat
       (nat.times x (nat.S nat.O))
       (__:(ne.eta nat.nat) => logic.eq nat.nat __ (nat.times x (nat.S nat.O)))
       (logic.refl nat.nat (nat.times x (nat.S nat.O)))
       x
       (nat.times_n_1 x)).


def divides_to_div_mod_spec :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O n)
             (ne.imp ne.i (divides n m) (div_mod.div_mod_spec m n (div_mod.div m n) nat.O)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posn:(ne.eps ne.i (nat.lt nat.O n)) =>
  _clearme:(ne.eps ne.i (divides n m)) =>
  match_divides_prop
    n
    m
    (div_mod.div_mod_spec m n (div_mod.div m n) nat.O)
    (q:(ne.eta nat.nat) =>
     eqm:(ne.eps ne.i (logic.eq nat.nat m (nat.times n q))) =>
     div_mod.div_mod_spec_intro
       m
       n
       (div_mod.div m n)
       nat.O
       posn
       (logic.eq_ind_r
          nat.nat
          (nat.times n q)
          (x:(ne.eta nat.nat) =>
           logic.eq nat.nat x (nat.plus (nat.times (div_mod.div x n) n) nat.O))
          (logic.eq_ind_r
             nat.nat
             (nat.times q n)
             (x:(ne.eta nat.nat) =>
              logic.eq nat.nat x (nat.plus (nat.times (div_mod.div x n) n) nat.O))
             (logic.eq_ind_r
                nat.nat
                q
                (x:(ne.eta nat.nat) =>
                 logic.eq nat.nat (nat.times q n) (nat.plus (nat.times x n) nat.O))
                (logic.rewrite_r
                   nat.nat
                   (nat.times n q)
                   (__:(ne.eta nat.nat) => logic.eq nat.nat __ (nat.plus (nat.times q n) nat.O))
                   (logic.rewrite_l
                      nat.nat
                      m
                      (__:(ne.eta nat.nat) =>
                       logic.eq nat.nat __ (nat.plus (nat.times q n) nat.O))
                      (logic.rewrite_r
                         nat.nat
                         (nat.times n q)
                         (__:(ne.eta nat.nat) => logic.eq nat.nat m (nat.plus __ nat.O))
                         (logic.rewrite_l
                            nat.nat
                            m
                            (__:(ne.eta nat.nat) => logic.eq nat.nat m (nat.plus __ nat.O))
                            (logic.rewrite_l
                               nat.nat
                               m
                               (__:(ne.eta nat.nat) => logic.eq nat.nat m __)
                               (logic.refl nat.nat m)
                               (nat.plus m nat.O)
                               (nat.plus_n_O m))
                            (nat.times n q)
                            eqm)
                         (nat.times q n)
                         (nat.commutative_times q n))
                      (nat.times n q)
                      eqm)
                   (nat.times q n)
                   (nat.commutative_times q n))
                (div_mod.div (nat.times q n) n)
                (div_mod.div_times q n posn))
             (nat.times n q)
             (nat.commutative_times n q))
          m
          eqm))
    _clearme.


def divides_to_mod_O :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O n)
             (ne.imp ne.i (divides n m) (logic.eq nat.nat (div_mod.mod m n) nat.O)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posn:(ne.eps ne.i (nat.lt nat.O n)) =>
  divnm:(ne.eps ne.i (divides n m)) =>
  div_mod.div_mod_spec_to_eq2
    m
    n
    (div_mod.div m n)
    (div_mod.mod m n)
    (div_mod.div m n)
    nat.O
    (div_mod.div_mod_spec_div_mod m n posn)
    (divides_to_div_mod_spec n m posn divnm).


def mod_O_to_divides :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O n)
             (ne.imp ne.i (logic.eq nat.nat (div_mod.mod m n) nat.O) (divides n m)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  auto:(ne.eps ne.i (nat.lt nat.O n)) =>
  auto':(ne.eps ne.i (logic.eq nat.nat (div_mod.mod m n) nat.O)) =>
  quotient
    n
    m
    (div_mod.div m n)
    (logic.rewrite_l
       nat.nat
       (nat.times n (div_mod.div m n))
       (__:(ne.eta nat.nat) => logic.eq nat.nat __ (nat.times n (div_mod.div m n)))
       (logic.refl nat.nat (nat.times n (div_mod.div m n)))
       m
       (logic.rewrite_r
          nat.nat
          (nat.minus m nat.O)
          (__:(ne.eta nat.nat) => logic.eq nat.nat (nat.times n (div_mod.div m n)) __)
          (logic.rewrite_l
             nat.nat
             (div_mod.mod m n)
             (__:(ne.eta nat.nat) =>
              logic.eq nat.nat (nat.times n (div_mod.div m n)) (nat.minus m __))
             (logic.rewrite_l
                nat.nat
                (nat.times (div_mod.div m n) n)
                (__:(ne.eta nat.nat) => logic.eq nat.nat __ (nat.minus m (div_mod.mod m n)))
                (div_mod.eq_times_div_minus_mod m n)
                (nat.times n (div_mod.div m n))
                (nat.commutative_times (div_mod.div m n) n))
             nat.O
             auto')
          m
          (nat.minus_n_O m))).


def divides_n_O :
  ne.eps ne.i (ne.forall ne.i nat.nat (n:(ne.eta nat.nat) => divides n nat.O))
  :=
  n:(ne.eta nat.nat) =>
  quotient
    n
    nat.O
    nat.O
    (logic.rewrite_r
       nat.nat
       (nat.times n nat.O)
       (__:(ne.eta nat.nat) => logic.eq nat.nat __ (nat.times n nat.O))
       (logic.refl nat.nat (nat.times n nat.O))
       nat.O
       (nat.times_n_O n)).


def divides_n_n :
  ne.eps ne.i (ne.forall ne.i nat.nat (n:(ne.eta nat.nat) => divides n n))
  :=
  n:(ne.eta nat.nat) => reflexive_divides n.


def eq_mod_to_divides :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.forall
             ne.i
             nat.nat
             (q:(ne.eta nat.nat) =>
              ne.imp
                ne.i
                (nat.lt nat.O q)
                (ne.imp
                   ne.i
                   (logic.eq nat.nat (div_mod.mod n q) (div_mod.mod m q))
                   (divides q (nat.minus n m)))))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  q:(ne.eta nat.nat) =>
  posq:(ne.eps ne.i (nat.lt nat.O q)) =>
  eqmod:(ne.eps ne.i (logic.eq nat.nat (div_mod.mod n q) (div_mod.mod m q))) =>
  nat.leb_elim
    n
    m
    (__:(ne.eta bool.bool) => divides q (nat.minus n m))
    (nm:(ne.eps ne.i (nat.le n m)) =>
     logic.eq_coerc
       (divides q nat.O)
       (divides q (nat.minus n m))
       (divides_n_O q)
       (logic.rewrite_r
          nat.nat
          nat.O
          (__:(ne.eta nat.nat) => logic.eq hole.o (divides q nat.O) (divides q __))
          (logic.refl hole.o (divides q nat.O))
          (nat.minus n m)
          (logic.sym_eq
             nat.nat
             nat.O
             (nat.minus n m)
             (logic.eq_coerc
                (logic.eq
                   nat.nat
                   (nat.minus nat.O (nat.minus m n))
                   (nat.minus (nat.plus nat.O n) m))
                (logic.eq nat.nat nat.O (nat.minus n m))
                (nat.minus_le_minus_minus_comm m n nat.O nm)
                (logic.rewrite_l
                   nat.nat
                   nat.O
                   (__:(ne.eta nat.nat) =>
                    logic.eq
                      hole.o
                      (logic.eq nat.nat __ (nat.minus (nat.plus nat.O n) m))
                      (logic.eq nat.nat nat.O (nat.minus n m)))
                   (logic.rewrite_l
                      nat.nat
                      n
                      (__:(ne.eta nat.nat) =>
                       logic.eq
                         hole.o
                         (logic.eq nat.nat nat.O (nat.minus __ m))
                         (logic.eq nat.nat nat.O (nat.minus n m)))
                      (logic.refl hole.o (logic.eq nat.nat nat.O (nat.minus n m)))
                      (nat.plus nat.O n)
                      (nat.plus_O_n n))
                   (nat.minus nat.O (nat.minus m n))
                   (nat.minus_O_n (nat.minus m n)))))))
    (nm:(ne.eps ne.i (ne.not ne.i (nat.le n m))) =>
     quotient
       q
       (nat.minus n m)
       (nat.minus (div_mod.div n q) (div_mod.div m q))
       (logic.eq_ind_r
          nat.nat
          (nat.minus (nat.times q (div_mod.div n q)) (nat.times q (div_mod.div m q)))
          (x:(ne.eta nat.nat) => logic.eq nat.nat (nat.minus n m) x)
          (logic.eq_ind_r
             nat.nat
             (nat.times (div_mod.div n q) q)
             (x:(ne.eta nat.nat) =>
              logic.eq nat.nat (nat.minus n m) (nat.minus x (nat.times q (div_mod.div m q))))
             (logic.eq_ind_r
                nat.nat
                (nat.times (div_mod.div m q) q)
                (x:(ne.eta nat.nat) =>
                 logic.eq nat.nat (nat.minus n m) (nat.minus (nat.times (div_mod.div n q) q) x))
                (logic.eq_ind_r
                   nat.nat
                   (nat.minus n (div_mod.mod n q))
                   (x:(ne.eta nat.nat) =>
                    logic.eq
                      nat.nat
                      (nat.minus n m)
                      (nat.minus x (nat.times (div_mod.div m q) q)))
                   (logic.eq_ind_r
                      nat.nat
                      (nat.minus n (nat.plus (div_mod.mod n q) (nat.times (div_mod.div m q) q)))
                      (x:(ne.eta nat.nat) => logic.eq nat.nat (nat.minus n m) x)
                      (logic.eq_ind_r
                         nat.nat
                         (div_mod.mod m q)
                         (x:(ne.eta nat.nat) =>
                          logic.eq
                            nat.nat
                            (nat.minus n m)
                            (nat.minus n (nat.plus x (nat.times (div_mod.div m q) q))))
                         (logic.eq_ind_r
                            nat.nat
                            (nat.plus (nat.times (div_mod.div m q) q) (div_mod.mod m q))
                            (x:(ne.eta nat.nat) =>
                             logic.eq nat.nat (nat.minus n m) (nat.minus n x))
                            (logic.eq_ind
                               nat.nat
                               m
                               (x_1:(ne.eta nat.nat) =>
                                logic.eq nat.nat (nat.minus n m) (nat.minus n x_1))
                               (logic.refl nat.nat (nat.minus n m))
                               (nat.plus (nat.times (div_mod.div m q) q) (div_mod.mod m q))
                               (div_mod.div_mod m q))
                            (nat.plus (div_mod.mod m q) (nat.times (div_mod.div m q) q))
                            (nat.commutative_plus
                               (div_mod.mod m q)
                               (nat.times (div_mod.div m q) q)))
                         (div_mod.mod n q)
                         eqmod)
                      (nat.minus
                         (nat.minus n (div_mod.mod n q))
                         (nat.times (div_mod.div m q) q))
                      (nat.minus_plus n (div_mod.mod n q) (nat.times (div_mod.div m q) q)))
                   (nat.times (div_mod.div n q) q)
                   (logic.rewrite_r
                      nat.nat
                      (nat.times q (div_mod.div n q))
                      (__:(ne.eta nat.nat) =>
                       logic.eq nat.nat __ (nat.minus n (div_mod.mod n q)))
                      (logic.rewrite_l
                         nat.nat
                         (nat.times q (div_mod.div n q))
                         (__:(ne.eta nat.nat) =>
                          logic.eq nat.nat (nat.times q (div_mod.div n q)) __)
                         (logic.refl nat.nat (nat.times q (div_mod.div n q)))
                         (nat.minus n (div_mod.mod n q))
                         (logic.rewrite_l
                            nat.nat
                            (nat.times (div_mod.div n q) q)
                            (__:(ne.eta nat.nat) =>
                             logic.eq nat.nat __ (nat.minus n (div_mod.mod n q)))
                            (div_mod.eq_times_div_minus_mod n q)
                            (nat.times q (div_mod.div n q))
                            (nat.commutative_times (div_mod.div n q) q)))
                      (nat.times (div_mod.div n q) q)
                      (nat.commutative_times (div_mod.div n q) q)))
                (nat.times q (div_mod.div m q))
                (nat.commutative_times q (div_mod.div m q)))
             (nat.times q (div_mod.div n q))
             (nat.commutative_times q (div_mod.div n q)))
          (nat.times q (nat.minus (div_mod.div n q) (div_mod.div m q)))
          (nat.distributive_times_minus q (div_mod.div n q) (div_mod.div m q)))).


def let_clause_1531 :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O m)
             (ne.imp
                ne.i
                (divides n m)
                (ne.forall
                   ne.i
                   nat.nat
                   (d:(ne.eta nat.nat) =>
                    ne.imp
                      ne.i
                      (logic.eq nat.nat m (nat.times n nat.O))
                      (logic.eq nat.nat m nat.O)))))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posm:(ne.eps ne.i (nat.lt nat.O m)) =>
  _clearme:(ne.eps ne.i (divides n m)) =>
  d:(ne.eta nat.nat) =>
  eqm:(ne.eps ne.i (logic.eq nat.nat m (nat.times n nat.O))) =>
  logic.rewrite_r
    nat.nat
    (nat.times n nat.O)
    (__:(ne.eta nat.nat) => logic.eq nat.nat m __)
    eqm
    nat.O
    (nat.times_n_O n).


def let_clause_15311 :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O m)
             (ne.imp
                ne.i
                (divides n m)
                (ne.forall
                   ne.i
                   nat.nat
                   (d:(ne.eta nat.nat) =>
                    ne.forall
                      ne.i
                      nat.nat
                      (p:(ne.eta nat.nat) =>
                       ne.imp
                         ne.i
                         (logic.eq nat.nat m (nat.times n (nat.S p)))
                         (logic.eq nat.nat m (nat.plus n (nat.times n p))))))))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posm:(ne.eps ne.i (nat.lt nat.O m)) =>
  _clearme:(ne.eps ne.i (divides n m)) =>
  d:(ne.eta nat.nat) =>
  p:(ne.eta nat.nat) =>
  eqm:(ne.eps ne.i (logic.eq nat.nat m (nat.times n (nat.S p)))) =>
  logic.rewrite_r
    nat.nat
    (nat.times n (nat.S p))
    (__:(ne.eta nat.nat) => logic.eq nat.nat m __)
    eqm
    (nat.plus n (nat.times n p))
    (nat.times_n_Sm n p).


def divides_to_le :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp ne.i (nat.lt nat.O m) (ne.imp ne.i (divides n m) (nat.le n m)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posm:(ne.eps ne.i (nat.lt nat.O m)) =>
  _clearme:(ne.eps ne.i (divides n m)) =>
  match_divides_prop
    n
    m
    (nat.le n m)
    (d:(ne.eta nat.nat) =>
     nat.match_nat_prop
       (__:(ne.eta nat.nat) => ne.imp ne.i (logic.eq nat.nat m (nat.times n __)) (nat.le n m))
       (eqm:(ne.eps ne.i (logic.eq nat.nat m (nat.times n nat.O))) =>
        logic.falsity
          (nat.le n m)
          (logic.absurd
             (nat.le (nat.S m) nat.O)
             (logic.eq_coerc
                (nat.le (nat.S nat.O) m)
                (nat.le (nat.S m) nat.O)
                posm
                (logic.rewrite_l
                   nat.nat
                   m
                   (__:(ne.eta nat.nat) =>
                    logic.eq hole.o (nat.le (nat.S __) m) (nat.le (nat.S m) nat.O))
                   (logic.rewrite_l
                      nat.nat
                      m
                      (__:(ne.eta nat.nat) =>
                       logic.eq hole.o (nat.le (nat.S m) m) (nat.le (nat.S m) __))
                      (logic.refl hole.o (nat.le (nat.S m) m))
                      nat.O
                      (let_clause_1531 n m posm _clearme d eqm))
                   nat.O
                   (let_clause_1531 n m posm _clearme d eqm)))
             (nat.not_le_Sn_O m)))
       (p:(ne.eta nat.nat) =>
        eqm:(ne.eps ne.i (logic.eq nat.nat m (nat.times n (nat.S p)))) =>
        logic.eq_ind_r
          nat.nat
          (nat.times n (nat.S p))
          (x:(ne.eta nat.nat) => nat.le n x)
          (logic.eq_coerc
             (nat.le n (nat.plus n (nat.times n p)))
             (nat.le n (nat.times n (nat.S p)))
             (nat.le_plus_n_r (nat.times n p) n)
             (logic.rewrite_l
                nat.nat
                (nat.plus n (nat.times n p))
                (__:(ne.eta nat.nat) =>
                 logic.eq hole.o (nat.le n (nat.plus n (nat.times n p))) (nat.le n __))
                (logic.rewrite_l
                   nat.nat
                   m
                   (__:(ne.eta nat.nat) =>
                    logic.eq hole.o (nat.le n (nat.plus n (nat.times n p))) (nat.le n __))
                   (logic.rewrite_l
                      nat.nat
                      m
                      (__:(ne.eta nat.nat) => logic.eq hole.o (nat.le n __) (nat.le n m))
                      (logic.refl hole.o (nat.le n m))
                      (nat.plus n (nat.times n p))
                      (let_clause_15311 n m posm _clearme d p eqm))
                   (nat.plus n (nat.times n p))
                   (let_clause_15311 n m posm _clearme d p eqm))
                (nat.times n (nat.S p))
                (nat.times_n_Sm n p)))
          m
          eqm)
       d)
    _clearme.


def dividesb :
  ne.eta (hole.arrow nat.nat (hole.arrow nat.nat bool.bool))
  :=
  n:(ne.eta nat.nat) => m:(ne.eta nat.nat) => nat.eqb (div_mod.mod m n) nat.O.


def dividesb_true_to_divides :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp ne.i (logic.eq bool.bool (dividesb n m) bool.true) (divides n m))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  logic.match_Or_prop
    (nat.lt nat.O n)
    (logic.eq nat.nat nat.O n)
    (ne.imp ne.i (logic.eq bool.bool (dividesb n m) bool.true) (divides n m))
    (posn:(ne.eps ne.i (nat.lt nat.O n)) =>
     divbnm:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.true)) =>
     mod_O_to_divides n m posn (nat.eqb_true_to_eq (div_mod.mod m n) nat.O divbnm))
    (eqnO:(ne.eps ne.i (logic.eq nat.nat nat.O n)) =>
     logic.eq_ind
       nat.nat
       nat.O
       (x_1:(ne.eta nat.nat) =>
        ne.imp ne.i (logic.eq bool.bool (dividesb x_1 m) bool.true) (divides x_1 m))
       (nat.sym_eq_match_nat_type_O
          nat.nat
          m
          (p:(ne.eta nat.nat) => div_mod.mod_aux m m p)
          (y:(ne.eta nat.nat) =>
           ne.imp ne.i (logic.eq bool.bool (nat.eqb y nat.O) bool.true) (divides nat.O m))
          (eqbmO:(ne.eps ne.i (logic.eq bool.bool (nat.eqb m nat.O) bool.true)) =>
           logic.eq_ind_r
             nat.nat
             nat.O
             (x:(ne.eta nat.nat) => divides nat.O x)
             (divides_n_n nat.O)
             m
             (nat.eqb_true_to_eq m nat.O eqbmO)))
       n
       eqnO)
    (nat.le_to_or_lt_eq nat.O n (nat.le_O_n n)).


def dividesb_false_to_not_divides :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (logic.eq bool.bool (dividesb n m) bool.false)
             (ne.not ne.i (divides n m)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  logic.match_Or_prop
    (nat.lt nat.O n)
    (logic.eq nat.nat nat.O n)
    (ne.imp
       ne.i
       (logic.eq bool.bool (dividesb n m) bool.false)
       (ne.not ne.i (divides n m)))
    (posn:(ne.eps ne.i (nat.lt nat.O n)) =>
     ndivbnm:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.false)) =>
     logic.not_to_not
       (divides n m)
       (logic.eq nat.nat (div_mod.mod m n) nat.O)
       (divides_to_mod_O n m posn)
       (nat.eqb_false_to_not_eq (div_mod.mod m n) nat.O ndivbnm))
    (eqnO:(ne.eps ne.i (logic.eq nat.nat nat.O n)) =>
     logic.eq_ind
       nat.nat
       nat.O
       (x_1:(ne.eta nat.nat) =>
        ne.imp
          ne.i
          (logic.eq bool.bool (dividesb x_1 m) bool.false)
          (ne.not ne.i (divides x_1 m)))
       (nat.sym_eq_match_nat_type_O
          nat.nat
          m
          (p:(ne.eta nat.nat) => div_mod.mod_aux m m p)
          (y:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (logic.eq bool.bool (nat.eqb y nat.O) bool.false)
             (ne.not ne.i (divides nat.O m)))
          (nat.nat_case
             m
             (__:(ne.eta nat.nat) =>
              ne.imp
                ne.i
                (logic.eq bool.bool (nat.eqb __ nat.O) bool.false)
                (ne.not ne.i (divides nat.O __)))
             (nat.sym_eq_eqb
                nat.O
                (y:(ne.eta (hole.arrow nat.nat bool.bool)) =>
                 ne.imp
                   ne.i
                   (logic.eq nat.nat m nat.O)
                   (ne.imp
                      ne.i
                      (logic.eq bool.bool (y nat.O) bool.false)
                      (ne.not ne.i (divides nat.O nat.O))))
                (nat.sym_eq_filter_nat_type_O
                   (hole.arrow nat.nat bool.bool)
                   nat.eqb_body
                   (y:(ne.eta (hole.arrow nat.nat bool.bool)) =>
                    ne.imp
                      ne.i
                      (logic.eq nat.nat m nat.O)
                      (ne.imp
                         ne.i
                         (logic.eq bool.bool (y nat.O) bool.false)
                         (ne.not ne.i (divides nat.O nat.O))))
                   (nat.sym_eq_eqb_body_O
                      (y:(ne.eta (hole.arrow nat.nat bool.bool)) =>
                       ne.imp
                         ne.i
                         (logic.eq nat.nat m nat.O)
                         (ne.imp
                            ne.i
                            (logic.eq bool.bool (y nat.O) bool.false)
                            (ne.not ne.i (divides nat.O nat.O))))
                      (nat.sym_eq_match_nat_type_O
                         bool.bool
                         bool.true
                         (q:(ne.eta nat.nat) => bool.false)
                         (y:(ne.eta bool.bool) =>
                          ne.imp
                            ne.i
                            (logic.eq nat.nat m nat.O)
                            (ne.imp
                               ne.i
                               (logic.eq bool.bool y bool.false)
                               (ne.not ne.i (divides nat.O nat.O))))
                         (auto:(ne.eps ne.i (logic.eq nat.nat m nat.O)) =>
                          auto':(ne.eps ne.i (logic.eq bool.bool bool.true bool.false)) =>
                          logic.not_to_not
                            (divides nat.O nat.O)
                            (logic.eq bool.bool bool.true bool.false)
                            (auto'':(ne.eps ne.i (divides nat.O nat.O)) =>
                             logic.rewrite_l
                               bool.bool
                               bool.true
                               (__:(ne.eta bool.bool) => logic.eq bool.bool bool.true __)
                               (logic.refl bool.bool bool.true)
                               bool.false
                               auto')
                            bool.not_eq_true_false)))))
             (a:(ne.eta nat.nat) =>
              __:(ne.eps ne.i (logic.eq nat.nat m (nat.S a))) =>
              _0:(ne.eps ne.i (logic.eq bool.bool (nat.eqb (nat.S a) nat.O) bool.false)) =>
              _clearme:(ne.eps ne.i (divides nat.O (nat.S a))) =>
              match_divides_prop
                nat.O
                (nat.S a)
                ne.false
                (q:(ne.eta nat.nat) =>
                 auto:(ne.eps ne.i (logic.eq nat.nat (nat.S a) (nat.times nat.O q))) =>
                 logic.absurd
                   (logic.eq nat.nat nat.O (nat.S a))
                   (logic.rewrite_r
                      nat.nat
                      n
                      (__1:(ne.eta nat.nat) => logic.eq nat.nat __1 (nat.S a))
                      (logic.rewrite_l
                         nat.nat
                         (nat.S a)
                         (__1:(ne.eta nat.nat) => logic.eq nat.nat __1 (nat.S a))
                         (logic.refl nat.nat (nat.S a))
                         n
                         (logic.rewrite_l
                            nat.nat
                            nat.O
                            (__1:(ne.eta nat.nat) => logic.eq nat.nat (nat.S a) __1)
                            (logic.rewrite_r
                               nat.nat
                               (nat.times q nat.O)
                               (__1:(ne.eta nat.nat) => logic.eq nat.nat (nat.S a) __1)
                               (logic.rewrite_l
                                  nat.nat
                                  (nat.times nat.O q)
                                  (__1:(ne.eta nat.nat) => logic.eq nat.nat (nat.S a) __1)
                                  auto
                                  (nat.times q nat.O)
                                  (nat.commutative_times nat.O q))
                               nat.O
                               (nat.times_n_O q))
                            n
                            eqnO))
                      nat.O
                      eqnO)
                   (nat.not_eq_O_S a))
                _clearme)))
       n
       eqnO)
    (nat.le_to_or_lt_eq nat.O n (nat.le_O_n n)).


def decidable_divides :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall ne.i nat.nat (m:(ne.eta nat.nat) => logic.decidable (divides n m))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  logic.match_Or_prop
    (logic.eq bool.bool (dividesb n m) bool.true)
    (logic.eq bool.bool (dividesb n m) bool.false)
    (logic.decidable (divides n m))
    (auto:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.true)) =>
     ne.or_il ne.i ne.i ne.i (divides n m) (ne.not ne.i (divides n m))
       (dividesb_true_to_divides
          n
          m
          (logic.rewrite_r
             bool.bool
             bool.true
             (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.true)
             (logic.refl bool.bool bool.true)
             (dividesb n m)
             auto)))
    (auto:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.false)) =>
     ne.or_ir ne.i ne.i ne.i (divides n m) (ne.not ne.i (divides n m))
       (dividesb_false_to_not_divides
          n
          m
          (logic.rewrite_r
             bool.bool
             bool.false
             (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.false)
             (logic.refl bool.bool bool.false)
             (dividesb n m)
             auto)))
    (bool.true_or_false (dividesb n m)).


def divides_to_dividesb_true :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O n)
             (ne.imp
                ne.i
                (divides n m)
                (logic.eq bool.bool (dividesb n m) bool.true)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posn:(ne.eps ne.i (nat.lt nat.O n)) =>
  divnm:(ne.eps ne.i (divides n m)) =>
  logic.match_Or_prop
    (logic.eq bool.bool (dividesb n m) bool.true)
    (logic.eq bool.bool (dividesb n m) bool.false)
    (logic.eq bool.bool (dividesb n m) bool.true)
    (auto:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.true)) =>
     logic.rewrite_r
       bool.bool
       bool.true
       (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.true)
       (logic.refl bool.bool bool.true)
       (dividesb n m)
       auto)
    (ndivbnm:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.false)) =>
     logic.falsity
       (logic.eq bool.bool (dividesb n m) bool.true)
       (logic.absurd
          (divides n m)
          divnm
          (dividesb_false_to_not_divides
             n
             m
             (logic.rewrite_r
                bool.bool
                bool.false
                (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.false)
                (logic.refl bool.bool bool.false)
                (dividesb n m)
                ndivbnm))))
    (bool.true_or_false (dividesb n m)).


def not_divides_to_dividesb_false :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (n:(ne.eta nat.nat) =>
        ne.forall
          ne.i
          nat.nat
          (m:(ne.eta nat.nat) =>
           ne.imp
             ne.i
             (nat.lt nat.O n)
             (ne.imp
                ne.i
                (ne.not ne.i (divides n m))
                (logic.eq bool.bool (dividesb n m) bool.false)))))
  :=
  n:(ne.eta nat.nat) =>
  m:(ne.eta nat.nat) =>
  posn:(ne.eps ne.i (nat.lt nat.O n)) =>
  logic.match_Or_prop
    (logic.eq bool.bool (dividesb n m) bool.true)
    (logic.eq bool.bool (dividesb n m) bool.false)
    (ne.imp
       ne.i
       (ne.not ne.i (divides n m))
       (logic.eq bool.bool (dividesb n m) bool.false))
    (divbnm:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.true)) =>
     ndivnm:(ne.eps ne.i (ne.not ne.i (divides n m))) =>
     logic.falsity
       (logic.eq bool.bool (dividesb n m) bool.false)
       (logic.absurd
          (divides n m)
          (dividesb_true_to_divides
             n
             m
             (logic.rewrite_r
                bool.bool
                bool.true
                (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.true)
                (logic.refl bool.bool bool.true)
                (dividesb n m)
                divbnm))
          ndivnm))
    (auto:(ne.eps ne.i (logic.eq bool.bool (dividesb n m) bool.false)) =>
     auto':(ne.eps ne.i (ne.not ne.i (divides n m))) =>
     logic.rewrite_r
       bool.bool
       bool.false
       (__:(ne.eta bool.bool) => logic.eq bool.bool __ bool.false)
       (logic.refl bool.bool bool.false)
       (dividesb n m)
       auto)
    (bool.true_or_false (dividesb n m)).


def prime :
  ne.eta (hole.arrow nat.nat hole.o)
  :=
  n:(ne.eta nat.nat) =>
  ne.and
    ne.i
    (nat.lt (nat.S nat.O) n)
    (ne.forall
       ne.i
       nat.nat
       (m:(ne.eta nat.nat) =>
        ne.imp
          ne.i
          (divides m n)
          (ne.imp ne.i (nat.lt (nat.S nat.O) m) (logic.eq nat.nat m n)))).


def prime_to_lt_O :
  ne.eps
    ne.i
    (ne.forall ne.i nat.nat (p:(ne.eta nat.nat) => ne.imp ne.i (prime p) (nat.lt nat.O p)))
  :=
  p:(ne.eta nat.nat) =>
  _clearme:(ne.eps ne.i (prime p)) =>
  logic.match_And_prop
    (nat.lt (nat.S nat.O) p)
    (ne.forall
       ne.i
       nat.nat
       (m:(ne.eta nat.nat) =>
        ne.imp
          ne.i
          (divides m p)
          (ne.imp ne.i (nat.lt (nat.S nat.O) m) (logic.eq nat.nat m p))))
    (nat.lt nat.O p)
    (lt1p:(ne.eps ne.i (nat.lt (nat.S nat.O) p)) =>
     auto:(ne.eps
             ne.i
             (ne.forall
                ne.i
                nat.nat
                (m:(ne.eta nat.nat) =>
                 ne.imp
                   ne.i
                   (divides m p)
                   (ne.imp ne.i (nat.lt (nat.S nat.O) m) (logic.eq nat.nat m p))))) =>
     nat.lt_S_to_lt nat.O p lt1p)
    _clearme.


def prime_to_lt_SO :
  ne.eps
    ne.i
    (ne.forall
       ne.i
       nat.nat
       (p:(ne.eta nat.nat) => ne.imp ne.i (prime p) (nat.lt (nat.S nat.O) p)))
  :=
  p:(ne.eta nat.nat) =>
  _clearme:(ne.eps ne.i (prime p)) =>
  logic.match_And_prop
    (nat.lt (nat.S nat.O) p)
    (ne.forall
       ne.i
       nat.nat
       (m:(ne.eta nat.nat) =>
        ne.imp
          ne.i
          (divides m p)
          (ne.imp ne.i (nat.lt (nat.S nat.O) m) (logic.eq nat.nat m p))))
    (nat.lt (nat.S nat.O) p)
    (lt1p:(ne.eps ne.i (nat.lt (nat.S nat.O) p)) =>
     auto:(ne.eps
             ne.i
             (ne.forall
                ne.i
                nat.nat
                (m:(ne.eta nat.nat) =>
                 ne.imp
                   ne.i
                   (divides m p)
                   (ne.imp ne.i (nat.lt (nat.S nat.O) m) (logic.eq nat.nat m p))))) =>
     lt1p)
    _clearme.


