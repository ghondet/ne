def reflexive :
  x:ne.set -> ne.eta (hole.arrow (hole.arrow x (hole.arrow x hole.o)) hole.o)
  :=
  A:ne.set =>
  R:(ne.eta (hole.arrow A (hole.arrow A hole.o))) => ne.forall ne.i A (x:(ne.eta A) => R x x).


def transitive :
  x:ne.set -> ne.eta (hole.arrow (hole.arrow x (hole.arrow x hole.o)) hole.o)
  :=
  A:ne.set =>
  R:(ne.eta (hole.arrow A (hole.arrow A hole.o))) =>
  ne.forall
    ne.i
    A
    (x:(ne.eta A) =>
     ne.forall
       ne.i
       A
       (y:(ne.eta A) =>
        ne.forall ne.i A (z:(ne.eta A) => ne.imp ne.i (R x y) (ne.imp ne.i (R y z) (R x z))))).


def RC :
  x:ne.set ->
  ne.eta (hole.arrow (hole.arrow x (hole.arrow x hole.o)) (hole.arrow x (hole.arrow x hole.o)))
  :=
  A:ne.set =>
  R:(ne.eta (hole.arrow A (hole.arrow A hole.o))) =>
  x:(ne.eta A) => y:(ne.eta A) => ne.or ne.i (R x y) (logic.eq A x y).


def RC_reflexive :
  x:ne.set ->
  ne.eps
    ne.i
    (ne.forall
       ne.i
       (hole.arrow x (hole.arrow x hole.o))
       (R:(ne.eta (hole.arrow x (hole.arrow x hole.o))) => reflexive x (RC x R)))
  :=
  A:ne.set =>
  R:(ne.eta (hole.arrow A (hole.arrow A hole.o))) =>
  x:(ne.eta A) => ne.or_ir ne.i ne.i ne.i (R x x) (logic.eq A x x) (logic.refl A x).


def injective :
  x:ne.set -> x0:ne.set -> ne.eta (hole.arrow (hole.arrow x x0) hole.o)
  :=
  A:ne.set =>
  B:ne.set =>
  f:(ne.eta (hole.arrow A B)) =>
  ne.forall
    ne.i
    A
    (x:(ne.eta A) =>
     ne.forall ne.i A (y:(ne.eta A) => ne.imp ne.i (logic.eq B (f x) (f y)) (logic.eq A x y))).


def commutative :
  x:ne.set -> ne.eta (hole.arrow (hole.arrow x (hole.arrow x x)) hole.o)
  :=
  A:ne.set =>
  f:(ne.eta (hole.arrow A (hole.arrow A A))) =>
  ne.forall ne.i A (x:(ne.eta A) => ne.forall ne.i A (y:(ne.eta A) => logic.eq A (f x y) (f y x))).


def associative :
  x:ne.set -> ne.eta (hole.arrow (hole.arrow x (hole.arrow x x)) hole.o)
  :=
  A:ne.set =>
  f:(ne.eta (hole.arrow A (hole.arrow A A))) =>
  ne.forall
    ne.i
    A
    (x:(ne.eta A) =>
     ne.forall
       ne.i
       A
       (y:(ne.eta A) => ne.forall ne.i A (z:(ne.eta A) => logic.eq A (f (f x y) z) (f x (f y z))))).


def monotonic :
  x:ne.set ->
  ne.eta (hole.arrow (hole.arrow x (hole.arrow x hole.o)) (hole.arrow (hole.arrow x x) hole.o))
  :=
  A:ne.set =>
  R:(ne.eta (hole.arrow A (hole.arrow A hole.o))) =>
  f:(ne.eta (hole.arrow A A)) =>
  ne.forall
    ne.i
    A
    (x:(ne.eta A) => ne.forall ne.i A (y:(ne.eta A) => ne.imp ne.i (R x y) (R (f x) (f y)))).


def distributive :
  x:ne.set ->
  ne.eta
    (hole.arrow (hole.arrow x (hole.arrow x x)) (hole.arrow (hole.arrow x (hole.arrow x x)) hole.o))
  :=
  A:ne.set =>
  f:(ne.eta (hole.arrow A (hole.arrow A A))) =>
  g:(ne.eta (hole.arrow A (hole.arrow A A))) =>
  ne.forall
    ne.i
    A
    (x:(ne.eta A) =>
     ne.forall
       ne.i
       A
       (y:(ne.eta A) =>
        ne.forall ne.i A (z:(ne.eta A) => logic.eq A (f x (g y z)) (g (f x y) (f x z))))).


