
(;;;;;;;;;;;;;;;;;;; Types ;;;;;;;;;;;;;;;;;;;)

iota     : ne.set.

(;;;;;;;;;; Propositional contents ;;;;;;;;;;;)

o : ne.set.
[] ne.eta o --> ne.preprop.

(;;;;;;;;;;;;;;;;; Functions ;;;;;;;;;;;;;;;;;)

arrow : ne.set -> ne.set -> ne.set.
[x,y] ne.eta (arrow x y) --> (ne.eta x) -> (ne.eta y).

(;;;;;;;;;;;;;;;;;;; Terms ;;;;;;;;;;;;;;;;;;;)

def eq  : a : ne.set -> ne.eta a -> ne.eta a -> ne.preprop.
select  : a : ne.set -> ne.eta (arrow (arrow a o) a).
witness : a : ne.set -> ne.eta a.
cond    : a : ne.set ->
                ne.eta (arrow o (arrow a (arrow a a))).

(;;;;;;;;;;;;;;;;; HOL proofs ;;;;;;;;;;;;;;;;)

REFL : a : ne.set ->
       t : ne.eta a ->
       ne.eps ne.c (eq a t t).

ABS_THM : a : ne.set ->
          b : ne.set ->
          f : (ne.eta a -> ne.eta b) ->
          g : (ne.eta a -> ne.eta b) ->
          (x : ne.eta a -> ne.eps ne.c (eq b (f x) (g x))) ->
          ne.eps ne.c (eq (arrow a b) f g).

APP_THM : a : ne.set ->
          b : ne.set ->
          f : ne.eta (arrow a b) ->
          g : ne.eta (arrow a b) ->
          x : ne.eta a -> y : ne.eta a ->
          (ne.eps ne.c (eq (arrow a b) f g)) ->
          (ne.eps ne.c (eq a x y)) ->
          (ne.eps ne.c (eq b (f x) (g y))).

PROP_EXT : p : ne.eta o ->
           q : ne.eta o ->
           (ne.eps ne.c q -> (ne.eps ne.c p)) ->
           (ne.eps ne.c p -> (ne.eps ne.c q)) ->
           (ne.eps ne.c (eq o p q)).

EQ_MP : p : ne.eta o ->
        q : ne.eta o ->
        (ne.eps ne.c (eq o p q)) ->
        (ne.eps ne.c p)->
        (ne.eps ne.c q).

def BETA_CONV : a : ne.set ->
                b : ne.set ->
                f : (ne.eta a -> ne.eta b) ->
                u : ne.eta a ->
                (ne.eps ne.c (eq b (f u) (f u))) :=
                a : ne.set =>
                b : ne.set =>
                f : (ne.eta a -> ne.eta b) =>
                u : ne.eta a =>
                REFL b (f u).

def SYM (a : ne.set)
        (t : ne.eta a)
        (u : ne.eta a)
        (H : ne.eps ne.c (eq a t u)) :
        ne.eps ne.c (eq a u t) :=
 EQ_MP
   (eq a t t)
   (eq a u t)
   (APP_THM
     a
     o
     (eq a t)
     (eq a u)
     t
       t
       (APP_THM
         a
         (arrow a o)
         (eq a)
         (eq a)
         t
           u
           (REFL (arrow a (arrow a o)) (eq a))
           H)
       (REFL a t))
   (REFL a t).

def TRANS (a : ne.set)
          (t : ne.eta a)
          (u : ne.eta a)
          (v : ne.eta a)
          (H1 : ne.eps ne.c (eq a t u))
          (H2 : ne.eps ne.c (eq a u v)) :
          ne.eps ne.c (eq a t v) :=
 EQ_MP
   (eq a u v)
   (eq a t v)
   (APP_THM
     a
     o
     (eq a u)
     (eq a t)
     v
       v
       (APP_THM a (arrow a o) (eq a) (eq a) u t (REFL (arrow a (arrow a o)) (eq a)) (SYM a t u H1))
       (REFL a v))
   H2.

def PROVE_HYP (x : ne.eta o)
              (y : ne.eta o)
              (H1 : ne.eps ne.c x)
              (H2 : ne.eps ne.c x -> (ne.eps ne.c y)) :
              ne.eps ne.c y := H2 H1.